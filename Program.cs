﻿
using System;

namespace C
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean retry = false;
            string[] options = new [] { "Проснувшись однажды утром после беспокойного сна, Грегор Замза обнаружил, что он у себя в постели превратился в страшное насекомое.", "Лежа на панцирнотвердой спине, он видел, стоило ему приподнять голову, свой коричневый, выпуклый, разделенный дугообразными чешуйками живот, на верхушке которого еле держалось готовое вот-вот окончательно сползти одеяло.", "Его многочисленные, убого тонкие по сравнению с остальным телом ножки беспомощно копошились у него перед глазами. «Что со мной случилось?» – подумал он. Это не было сном.", "Его комната, настоящая, разве что слишком маленькая, но обычная комната, мирно покоилась в своих четырех хорошо знакомых стенах." };
            do {
                Console.WriteLine("Нажмите Enter");
                string e = Console.ReadLine();

                DateTime startedAt = DateTime.Now;
                Random random = new Random();
                string text = options[random.Next(options.Length)];
                double textLength = text.Length; 

                Console.WriteLine($"Повторите следующий текст: \n{text}");
                string userText = Console.ReadLine();
                TimeSpan span = DateTime.Now - startedAt;

                double speed = textLength/span.TotalMinutes;
                Console.WriteLine($"Ваша скорость: {speed} символов в минуту");

                Console.WriteLine("Хотите попробовать еще раз? y/n");

                string wannaRetry = Console.ReadLine();
                retry = wannaRetry == "y";
            } while (retry);
            
        }
    }
}
